#!/usr/bin/env zsh

function __kk::os::archlinux() {
  echo "ARCH" > /dev/null
}

typeset -A __kk_os_release

[ -f /etc/os-release ] && {
  eval "__kk_os_release=("${$(</etc/os-release)//=/ }")"
}

case "${__kk_os_release[NAME]}" in
  "Arch Linux")
    __kk::os::archlinux
    ;;
esac

